/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#include <QApplication>
#include <QTranslator>

#include <QtGui>

#include "TU_Main.h"

int main(int argc, char *argv[])
{
#ifdef Q_OS_MACX
    if ( QSysInfo::MacintoshVersion > QSysInfo::MV_10_8 )
    {
        // fix Mac OS X 10.9 (mavericks) font issue
        // https://bugreports.qt-project.org/browse/QTBUG-32789
        QFont::insertSubstitution(".Lucida Grande UI", "Lucida Grande");
    }
#endif
	
    QApplication app(argc, argv);
	
	/*QString locale = QLocale::system().name();
	
	QTranslator translator;
	translator.load(QString(":/translations/TuxUnits_") + locale);
	app.installTranslator(&translator);*/

    TU_MainWindow *window = new TU_MainWindow ();
    window->show ();

    return app.exec();
}
