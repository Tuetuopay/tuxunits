/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#include "TU_Main.h"

TU_MainWindow::TU_MainWindow ( QWidget *parent ) : QLabel ( parent )
{
	this->setFixedSize ( 639, 299 );
	this->setPixmap ( QPixmap ( ":/images/background.png" ));
	
	this->labelHeader = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">Choisir ou changer d'unit�s :</span>" ));
	
	this->btnHome = new QPushButton ( tr ( "Accueil" ));
	this->btnMesures = new QPushButton ( tr ( "Mesures" ));
	this->btnVolumes = new QPushButton ( tr ( "Volumes" ));
	this->btnCurrencies = new QPushButton ( tr ( "Mon�taires" ));
	this->btnTime = new QPushButton ( tr ( "Temps" ));
	this->btnTemps = new QPushButton ( tr ( "Temp�ratures" ));
	
	this->layHeaderButtons = new QHBoxLayout ();
	this->layHeaderButtons->setSpacing ( 5 );
	this->layHeaderButtons->addWidget ( this->btnHome );
	this->layHeaderButtons->addWidget ( this->btnMesures );
	this->layHeaderButtons->addWidget ( this->btnVolumes );
	this->layHeaderButtons->addWidget ( this->btnCurrencies );
	this->layHeaderButtons->addWidget ( this->btnTime );
	this->layHeaderButtons->addWidget ( this->btnTemps );
	
	this->vSpacer = new QSpacerItem ( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
	
	this->layHeader = new QVBoxLayout ();
	this->layHeader->setContentsMargins(2, 5, 2, 0);
	this->layHeader->addWidget ( this->labelHeader );
	this->layHeader->addLayout ( this->layHeaderButtons );
	this->layHeader->addItem ( this->vSpacer );
	
	this->header = new QWidget ( this );
	this->header->setFixedSize ( 639, 100 );
	this->header->setLayout ( this->layHeader );	
	this->header->move ( 0,0 );
	this->btnHome->setEnabled ( false );
	this->header->show ();
	
		// -------- d�claration des widgets de cat�gories --------- //
	this->catHome = new TU_Home ( this );
	this->catMesures = new TU_Category ( this, CAT_MESURES );
	this->catVolumes = new TU_Category ( this, CAT_VOLUMES );
	this->catCurrencies = new TU_Category ( this, CAT_CURRENCIES );
	this->catTime = new TU_Category ( this, CAT_TIME );
	this->catTemps = new TU_Category ( this, CAT_TEMPS );
	this->catAbout = new TU_About ( this );
	
	this->catMesures->move ( 500, 500 );
	this->catVolumes->move ( 500, 500 );
	this->catCurrencies->move ( 500, 500 );
	this->catTime->move ( 500, 500 );
	this->catTemps->move ( 500, 500 );
	this->catAbout->move ( 500, 500 );
	
	this->catMiddlePlace = new QPoint ( this->width () / 2 - this->catMesures->width () / 2,
									   (this->height () - 74) / 2 - this->catMesures->height () / 2 + 74 );
	this->catLeftPlace = new QPoint ( this->catMesures->width () * -1, this->catMiddlePlace->y () );
	this->catRightPlace = new QPoint ( this->width (), this->catMiddlePlace->y () );
	
	this->catHome->move ( *(this->catMiddlePlace) );
	
		// -------- d�claration des masques de c�t�s ---------- //
	this->labelMaskLeft = new QLabel ( this );
	this->labelMaskLeft->setPixmap ( QPixmap ( ":/images/hover_left.png" ));
	this->labelMaskLeft->move ( 0, 72 );
	
	this->labelMaskRight = new QLabel ( this );
	this->labelMaskRight->setPixmap ( QPixmap ( ":/images/hover_right.png" ));
	this->labelMaskRight->move ( 588, 72 );
	
	QObject::connect ( btnHome, SIGNAL(clicked()), this, SLOT(btnHome_clicked()) );
	QObject::connect ( btnMesures, SIGNAL(clicked()), this, SLOT(btnMesures_clicked()) );
	QObject::connect ( btnVolumes, SIGNAL(clicked()), this, SLOT(btnVolumes_clicked()) );
	QObject::connect ( btnCurrencies, SIGNAL(clicked()), this, SLOT(btnCurrencies_clicked()) );
	QObject::connect ( btnTime, SIGNAL(clicked()), this, SLOT(btnTime_clicked()) );
	QObject::connect ( btnTemps, SIGNAL(clicked()), this, SLOT(btnTemps_clicked()) );
	QObject::connect ( catCurrencies, SIGNAL(toggleCurrencies(bool)), this, SLOT(setShown(bool)) );
	QObject::connect ( catHome, SIGNAL(aboutClicked()), this, SLOT(btnAbout_clicked()) );
	
	this->setWindowTitle ( "TuxUnits v0.1" );
	
	this->animCurve = QEasingCurve::OutBounce;
}

TU_MainWindow::~TU_MainWindow ()
{
	
}

void TU_MainWindow::btnHome_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	animation->addAnimation ( animLToMCat ( this->catHome ) );
	if ( !this->btnMesures->isEnabled () )
		animation->addAnimation ( animMToRCat ( this->catMesures ) );
	else if ( !this->btnVolumes->isEnabled () )
		animation->addAnimation ( animMToRCat ( this->catVolumes ) );
	else if ( !this->btnCurrencies->isEnabled () )
		animation->addAnimation ( animMToRCat ( this->catCurrencies ) );
	else if ( !this->btnTime->isEnabled () )
		animation->addAnimation ( animMToRCat ( this->catTime ) );
	else if ( !this->btnTemps->isEnabled () )
		animation->addAnimation ( animMToRCat ( this->catTemps ) );
	else if ( !this->btnAboutEnabled )
		animation->addAnimation ( animMToRCat ( this->catAbout ) );
	
	this->animation->start ();
	
	QObject::connect ( this->animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( false );
	this->btnMesures->setEnabled ( true );
	this->btnVolumes->setEnabled ( true );
	this->btnCurrencies->setEnabled ( true );
	this->btnTime->setEnabled ( true );
	this->btnTemps->setEnabled ( true );
	this->catHome->toggleAbout ( true );
	btnAboutEnabled = true;
}

void TU_MainWindow::btnMesures_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	if ( !this->btnHome->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catHome ) );
		animation->addAnimation ( animRToMCat ( this->catMesures ) );
	}
	else if ( !this->btnVolumes->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catMesures ) );
		animation->addAnimation ( animMToRCat ( this->catVolumes ) );
	}
	else if ( !this->btnCurrencies->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catMesures ) );
		animation->addAnimation ( animMToRCat ( this->catCurrencies ) );
	}
	else if ( !this->btnTime->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catMesures ) );
		animation->addAnimation ( animMToRCat ( this->catTime ) );
	}
	else if ( !this->btnTemps->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catMesures ) );
		animation->addAnimation ( animMToRCat ( this->catTemps ) );
	}
	else if ( !this->btnAboutEnabled )	{
		animation->addAnimation ( animLToMCat ( this->catMesures ) );
		animation->addAnimation ( animMToRCat ( this->catAbout ) );
	}
	
	this->animation->start ();
	
	QObject::connect ( this->animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( true );
	this->btnMesures->setEnabled ( false );
	this->btnVolumes->setEnabled ( true );
	this->btnCurrencies->setEnabled ( true );
	this->btnTime->setEnabled ( true );
	this->btnTemps->setEnabled ( true );
	this->catHome->toggleAbout ( true );
	btnAboutEnabled= true;
}

void TU_MainWindow::btnVolumes_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	if ( !this->btnHome->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catHome ) );
		animation->addAnimation ( animRToMCat ( this->catVolumes ) );
	}
	else if ( !this->btnMesures->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catMesures ) );
		animation->addAnimation ( animRToMCat ( this->catVolumes ) );
	}
	else if ( !this->btnCurrencies->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catVolumes ) );
		animation->addAnimation ( animMToRCat ( this->catCurrencies ) );
	}
	else if ( !this->btnTime->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catVolumes ) );
		animation->addAnimation ( animMToRCat ( this->catTime ) );
	}
	else if ( !this->btnTemps->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catVolumes ) );
		animation->addAnimation ( animMToRCat ( this->catTemps ) );
	}
	else if ( !this->btnAboutEnabled )	{
		animation->addAnimation ( animLToMCat ( this->catVolumes ) );
		animation->addAnimation ( animMToRCat ( this->catAbout ) );
	}
	
	this->animation->start ();
	
	QObject::connect ( this->animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( true );
	this->btnMesures->setEnabled ( true );
	this->btnVolumes->setEnabled ( false );
	this->btnCurrencies->setEnabled ( true );
	this->btnTime->setEnabled ( true );
	this->btnTemps->setEnabled ( true );
	this->catHome->toggleAbout ( true );
	btnAboutEnabled = true;
}

void TU_MainWindow::btnCurrencies_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	if ( !this->btnHome->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catHome ) );
		animation->addAnimation ( animRToMCat ( this->catCurrencies ) );
	}
	else if ( !this->btnMesures->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catMesures ) );
		animation->addAnimation ( animRToMCat ( this->catCurrencies ) );
	}
	else if ( !this->btnVolumes->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catVolumes ) );
		animation->addAnimation ( animRToMCat ( this->catCurrencies ) );
	}
	else if ( !this->btnTime->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catCurrencies ) );
		animation->addAnimation ( animMToRCat ( this->catTime ) );
	}
	else if ( !this->btnTemps->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catCurrencies ) );
		animation->addAnimation ( animMToRCat ( this->catTemps ) );
	}
	else if ( !this->btnAboutEnabled )	{
		animation->addAnimation ( animLToMCat ( this->catCurrencies ) );
		animation->addAnimation ( animMToRCat ( this->catAbout ) );
	}
	
	this->animation->start ();
	
	QObject::connect ( this->animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( true );
	this->btnMesures->setEnabled ( true );
	this->btnVolumes->setEnabled ( true );
	this->btnCurrencies->setEnabled ( false );
	this->btnTime->setEnabled ( true );
	this->btnTemps->setEnabled ( true );
	this->catHome->toggleAbout ( true );
	btnAboutEnabled = true;
}

void TU_MainWindow::btnTime_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	if ( !this->btnHome->isEnabled () ){
		animation->addAnimation ( animMToLCat ( this->catHome ) );
		animation->addAnimation ( animRToMCat ( this->catTime ) );
	}
	if ( !this->btnMesures->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catMesures ) );
		animation->addAnimation ( animRToMCat ( this->catTime ) );
	}
	else if ( !this->btnVolumes->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catVolumes ) );
		animation->addAnimation ( animRToMCat ( this->catTime ) );
	}
	else if ( !this->btnCurrencies->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catCurrencies ) );
		animation->addAnimation ( animRToMCat ( this->catTime ) );
	}
	else if ( !this->btnTemps->isEnabled () )	{
		animation->addAnimation ( animLToMCat ( this->catTime ) );
		animation->addAnimation ( animMToRCat ( this->catTemps ) );
	}
	else if ( !this->btnAboutEnabled )	{
		animation->addAnimation ( animLToMCat ( this->catTime ) );
		animation->addAnimation ( animMToRCat ( this->catAbout ) );
	}
	
	this->animation->start ();
	
	QObject::connect ( this->animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( true );
	this->btnMesures->setEnabled ( true );
	this->btnVolumes->setEnabled ( true );
	this->btnCurrencies->setEnabled ( true );
	this->btnTime->setEnabled ( false );
	this->btnTemps->setEnabled ( true );
	this->catHome->toggleAbout ( true );
	btnAboutEnabled = true;
}

void TU_MainWindow::btnTemps_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	if ( !this->btnHome->isEnabled () ){
		animation->addAnimation ( animMToLCat ( this->catHome ) );
		animation->addAnimation ( animRToMCat ( this->catTemps ) );
	}
	else if ( !this->btnMesures->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catMesures ) );
		animation->addAnimation ( animRToMCat ( this->catTemps ) );
	}
	else if ( !this->btnVolumes->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catVolumes ) );
		animation->addAnimation ( animRToMCat ( this->catTemps ) );
	}
	else if ( !this->btnCurrencies->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catCurrencies ) );
		animation->addAnimation ( animRToMCat ( this->catTemps ) );
	}
	else if ( !this->btnTime->isEnabled () )	{
		animation->addAnimation ( animMToLCat ( this->catTime ) );
		animation->addAnimation ( animRToMCat ( this->catTemps ) );
	}
	else if ( !this->btnAboutEnabled )	{
		animation->addAnimation ( animLToMCat ( this->catTemps ) );
		animation->addAnimation ( animMToRCat ( this->catAbout ) );
	}
	
	this->animation->start ();
	
	QObject::connect ( this->animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( true );
	this->btnMesures->setEnabled ( true );
	this->btnVolumes->setEnabled ( true );
	this->btnCurrencies->setEnabled ( true );
	this->btnTime->setEnabled ( true );
	this->btnTemps->setEnabled ( false );
	this->catHome->toggleAbout ( true );
	btnAboutEnabled = true;
}

void TU_MainWindow::btnAbout_clicked ()
{
	this->animation = new QParallelAnimationGroup;
	animation->addAnimation ( animRToMCat ( this->catAbout ) );
	if ( !this->btnHome->isEnabled () )
		animation->addAnimation ( animMToLCat ( this->catHome ) );
	if ( !this->btnMesures->isEnabled () )
		animation->addAnimation ( animMToLCat ( this->catMesures ) );
	else if ( !this->btnVolumes->isEnabled () )
		animation->addAnimation ( animMToLCat ( this->catVolumes ) );
	else if ( !this->btnCurrencies->isEnabled () )
		animation->addAnimation ( animMToLCat ( this->catCurrencies ) );
	else if ( !this->btnTime->isEnabled () )
		animation->addAnimation ( animMToLCat ( this->catTime ) );
	else if ( !this->btnTemps->isEnabled () )
		animation->addAnimation ( animMToLCat ( this->catTemps ) );
	
	this->animation->start ();
	
	QObject::connect ( animation, SIGNAL(finished()), this, SLOT(animationFinished()) );
	
	this->btnHome->setEnabled ( true );
	this->btnMesures->setEnabled ( true );
	this->btnVolumes->setEnabled ( true );
	this->btnCurrencies->setEnabled ( true );
	this->btnTime->setEnabled ( true );
	this->btnTemps->setEnabled ( true );
	this->catHome->toggleAbout ( false );
	btnAboutEnabled = false;
}

void TU_MainWindow::animationFinished ()
{
	delete this->animation;
}

QPropertyAnimation *TU_MainWindow::animLToMCat ( QWidget *leftCat )
{
	QPropertyAnimation *animLeftWidget = new QPropertyAnimation ( leftCat, "pos" );
	animLeftWidget->setDuration ( 1000 );
	animLeftWidget->setEasingCurve( this->animCurve );
	animLeftWidget->setStartValue ( *(this->catLeftPlace) );
	animLeftWidget->setEndValue ( *(this->catMiddlePlace) );
	return animLeftWidget;
}

QPropertyAnimation *TU_MainWindow::animMToRCat ( QWidget *rightCat )
{
	QPropertyAnimation *animRightWidget = new QPropertyAnimation ( rightCat, "pos" );
	animRightWidget->setDuration ( 1000 );
	animRightWidget->setEasingCurve ( this->animCurve );
	animRightWidget->setStartValue ( *(this->catMiddlePlace) );
	animRightWidget->setEndValue ( *(this->catRightPlace) );
	return animRightWidget;
}

QPropertyAnimation *TU_MainWindow::animMToLCat ( QWidget *leftCat )
{
	QPropertyAnimation *animLeftWidget = new QPropertyAnimation ( leftCat, "pos" );
	animLeftWidget->setDuration ( 1000 );
	animLeftWidget->setEasingCurve ( this->animCurve );
	animLeftWidget->setStartValue ( *(this->catMiddlePlace) );
	animLeftWidget->setEndValue ( *(this->catLeftPlace) );
	return animLeftWidget;
}

QPropertyAnimation *TU_MainWindow::animRToMCat ( QWidget *leftCat )
{
	QPropertyAnimation *animRightWidget = new QPropertyAnimation ( leftCat, "pos" );
	animRightWidget->setDuration ( 1000 );
	animRightWidget->setEasingCurve ( this->animCurve );
	animRightWidget->setStartValue ( *(this->catRightPlace) );
	animRightWidget->setEndValue ( *(this->catMiddlePlace) );
	return animRightWidget;
}





















