######################################################################
# Automatically generated by qmake (2.01a) jeu. sept. 22 22:34:21 2011
######################################################################

TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += .

# Input
HEADERS += TU_Main.h \
           TU_Category.h \
           TU_Home.h \
           TU_About.h
SOURCES += main.cpp \
           TU_Main.cpp \
           TU_Category.cpp \
           TU_Home.cpp \
           TU_About.cpp
RESOURCES += resource.qrc
QT += network
