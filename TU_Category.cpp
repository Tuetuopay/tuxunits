/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#include "TU_Category.h"

TU_Category::TU_Category ( QWidget *parent, int selectedCategory ) : QLabel ( parent )
{
	this->setFixedSize ( 536, 180 );
	this->setPixmap ( QPixmap ( ":/images/background-categories.png" ));
	
	this->lblInputUnit = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">unit� d'entr�e :</span>" ) );
	this->lblOutputUnit = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">convertir vers :</span>" ) );
	
	this->editInput = new QLineEdit ();
	this->editInput->setPlaceholderText ( tr ( "Valeur d'entr�e" ));
	this->editOutput = new QLineEdit ();
	this->editOutput->setPlaceholderText ( tr ( "Valeur de sortie" ));
	
	this->comboInputUnit = new QComboBox ();
	this->comboOutputUnit = new QComboBox ();
	
	this->btnCalculate = new QPushButton ( tr ( "Calculer" ));
	
	this->lblPicture = new QLabel ();
	
	this->spacerHLeft = new QSpacerItem( 40, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
	this->spacerHMiddle = new QSpacerItem( 40, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
	this->spacerHRight = new QSpacerItem( 40, 1, QSizePolicy::Expanding, QSizePolicy::Minimum );
	this->spacerVMiddle = new QSpacerItem( 20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding );
	
	this->layMain = new QGridLayout ();
	
	this->category = selectedCategory;
	
	switch ( this->category ) {
		case CAT_MESURES:
			buildMesures ();
			break;
		case CAT_VOLUMES:
			buildVolumes ();
			break;
		case CAT_CURRENCIES:
			buildCurrencies ();
			break;
		case CAT_TIME:
			buildTime ();
			break;
		case CAT_TEMPS:
			buildTemps ();
			break;

		default:
			break;
	}
	
	this->layMain->setContentsMargins(8, 5, 8, 0);
	this->layMain->setVerticalSpacing(0);
	
	this->layMain->addWidget ( this->lblTitle, 0, 0, 1, 3 );
	this->layMain->addWidget ( this->lblInputUnit, 1, 1, 1, 1 );
	this->layMain->addWidget ( this->lblOutputUnit, 1, 2, 1, 1 );
	this->layMain->addWidget ( this->editInput, 2, 0, 1, 1 );
	this->layMain->addWidget ( this->comboInputUnit, 2, 1, 1, 1 );
	this->layMain->addWidget ( this->comboOutputUnit, 2, 2, 1, 1 );
	this->layMain->addWidget ( this->lblPicture, 3, 0, 3, 1 );
	this->layMain->addWidget ( this->btnCalculate, 3, 1, 1, 1 );
	this->layMain->addWidget ( this->editOutput, 4, 1, 1, 1 );
	this->layMain->addItem ( this->spacerVMiddle, 5, 1, 1, 1 );
	this->layMain->addItem ( this->spacerHLeft, 6, 0, 1, 1 );
	this->layMain->addItem ( this->spacerHMiddle, 6, 1, 1, 1 );
	this->layMain->addItem ( this->spacerHRight, 6, 2, 1, 1 );
	
	this->layMain->setColumnMinimumWidth ( 0, 176 );
	this->layMain->setColumnMinimumWidth ( 1, 176 );
	this->layMain->setColumnMinimumWidth ( 2, 176 );
	
	this->setLayout ( this->layMain );
	
	this->actualPos.setX (0);
	this->actualPos.setY (0);
	
	QObject::connect ( this->btnCalculate, SIGNAL(clicked()), this, SLOT(btnCalculate_clicked()) );
}

TU_Category::~TU_Category ()
{}

void TU_Category::buildMesures ()
{
	this->lblTitle = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">Unit�s de mesure de distances :</span>" ));
	this->lblPicture->setPixmap ( QPixmap ( ":/images/rulers.png" ));
	
	QStringList items;
	items << tr ( "Microm�tre (�m)" )
		<< tr ( "Ligne (Angaise Actuelle)" )
		<< tr ( "Point" )
		<< tr ( "Pouce (in)" )
		<< tr ( "Pied (ft)" )
		<< tr ( "Yard" )
		<< tr ( "Centim�tres (cm)" )
		<< tr ( "M�tres (m)" )
		<< tr ( "Kilom�tres (km)" )
		<< tr ( "Mille Nautique" )
		<< tr ( "Lieue Anglaise" )
		<< tr ( "Unit� Astronomique (UA)" )
		<< tr ( "Ann�e Lumi�re (AL)" );
	
	this->comboInputUnit->clear ();
	this->comboInputUnit->insertItems ( 0, items );
	this->comboOutputUnit->clear ();
	this->comboOutputUnit->insertItems ( 0, items );
}

void TU_Category::buildVolumes ()
{
	this->lblTitle = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">Unit�s de volume :</span>" ));
	this->lblPicture->setPixmap ( QPixmap ( ":/images/volumes.png" ));
	
	QStringList items;
	items << tr ( "Cuill�re � Caf�" )
		<< tr ( "Centilitre (cL)" )
		<< tr ( "Cuill�re � Soupe" )
		<< tr ( "Shaku Japonais" )
		<< tr ( "Fl. Oz" )
		<< tr ( "Gill Am�ricain" )
		<< tr ( "Gill Anglais" )
		<< tr ( "D�cilitre (dL)" )
		<< tr ( "Pinte" )
		<< tr ( "Litre (L)" )
		<< tr ( "D�cim�tre Cube (dm�)" )
		<< tr ( "Quart" )
		<< tr ( "Gallon (gal)" )
		<< tr ( "M�tre Cube (m�)" )
		<< tr ( "D�cam�tre Cube (dam�)" );
	
	this->comboInputUnit->clear ();
	this->comboInputUnit->insertItems ( 0, items );
	this->comboOutputUnit->clear ();
	this->comboOutputUnit->insertItems ( 0, items );
}

void TU_Category::buildCurrencies ()
{
	this->lblTitle = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">Unit�s mon�taires :</span>" ));
	this->lblPicture->setPixmap ( QPixmap ( ":/images/wallet-1.png" ));
	
	QStringList items;
	items << tr ( "USD Dollar Am�ricain" )
		<< tr ( "EUR Euro" )
		<< tr ( "GBP Livre Sterling" )
		<< tr ( "CYN Yuan Chinois" )
		<< tr ( "JPY Yen Japonais" )
		<< tr ( "CAD Dollar Canadien" )
		<< tr ( "CHF Franc Suisse" )
		<< tr ( "AUD Dollar Australien" )
		<< tr ( "INR Roupie Indienne" )
		<< tr ( "NZD Dollar N�o-Z�landais" )
		<< tr ( "ZAR Rands Dud-Africains" )
		<< tr ( "ILS Nouveaux-Shekels Isra�liens" )
		<< tr ( "XAU Onces d'Or" )
		<< tr ( "XPT Onces de Platine" )
		<< tr ( "XPD Onces de Palladium" );
	
	this->comboInputUnit->clear ();
	this->comboInputUnit->insertItems ( 0, items );
	this->comboOutputUnit->clear ();
	this->comboOutputUnit->insertItems ( 0, items );
	
	this->urlCur = new QUrl;
	this->requestCur = new QNetworkRequest;
	this->networkManager = new QNetworkAccessManager;
}

void TU_Category::buildTime ()
{
	this->lblTitle = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">Unit�s de temps :</span>" ));
	this->lblPicture->setPixmap ( QPixmap ( ":/images/time.png" ));
	
	QStringList items;
	items << tr ( "Secondes (s)" )
		<< tr ( "Minutes (min)" )
		<< tr ( "Heures (H)" )
		<< tr ( "Jour" )
		<< tr ( "Mois" )
		<< tr ( "Ann�e" )
		<< tr ( "D�cennie" )
		<< tr ( "Si�cle (S)" )
		<< tr ( "Mill�naire" );
	
	this->comboInputUnit->clear ();
	this->comboInputUnit->insertItems ( 0, items );
	this->comboOutputUnit->clear ();
	this->comboOutputUnit->insertItems ( 0, items );
}

void TU_Category::buildTemps ()
{
	this->lblTitle = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">Unit�s de temp�rature :</span>" ));
	this->lblPicture->setPixmap ( QPixmap ( ":/images/thermometer.png" ));
	
	QStringList items;
	items << tr ( "Celsius" )
		<< tr ( "Fahrenheit" )
		<< tr ( "kelvin" );
	
	this->comboInputUnit->clear ();
	this->comboInputUnit->insertItems ( 0, items );
	this->comboOutputUnit->clear ();
	this->comboOutputUnit->insertItems ( 0, items );
}

void TU_Category::btnCalculate_clicked()
{
	switch ( this->category ) {
		case CAT_MESURES:
			calcMesures ();
			break;
		case CAT_VOLUMES:
			calcVolumes ();
			break;
		case CAT_CURRENCIES:
			calcCurrencies ();
			break;
		case CAT_TIME:
			calcTime ();
			break;
		case CAT_TEMPS:
			calcTemps ();
			break;

		default:
			break;
	}
}

void TU_Category::calcMesures ()
{
	double refToMetre[] = { 0.000001, 0.002255, 0.00188, 0.0254, 0.3408, 0.9144, 0.01, 1.0, 1000.0, 1852.0, 3898.0, 800000000, 9460730000000000 };
	bool ok = true;
	double result = this->editInput->text ().toDouble ( &ok );
	
	if ( !ok )	{
		QMessageBox::critical ( this, tr ( "Erreur" ), tr ( "Saisie non valide." ) );
		return;
	}
	
	result *= refToMetre[this->comboInputUnit->currentIndex ()];
	result /= refToMetre[this->comboOutputUnit->currentIndex ()];
	
	this->editOutput->setText ( QString ("%1").arg ( result ));
}

void TU_Category::calcVolumes ()
{
	double refToLitre[] = { 0.004929022082018927, 0.01, 0.01478677470870054, 0.01803914494452963, 0.02957354941740108, 0.1182941976696043, 0.1420656343230573, 0.1,
							0.5682463916354131, 1.0, 1.0, 1.136621959536258, 3.785441193171064, 1000.0, 1000000.0 };
	bool ok = true;
	double result = this->editInput->text ().toDouble ( &ok );
	
	if ( !ok )	{
		QMessageBox::critical ( this, tr ( "Erreur" ), tr ( "Saisie non valide." ) );
		return;
	}
	
	result *= refToLitre[this->comboInputUnit->currentIndex ()];
	result /= refToLitre[this->comboOutputUnit->currentIndex ()];
	
	this->editOutput->setText ( QString ("%1").arg ( result ));
}

void TU_Category::calcCurrencies ()
{
	emit toggleCurrencies ( false );
	
	this->curProgress = new QProgressDialog ( tr ( "R�cup�ration des taux de change..." ), tr ( "Annuler" ), 0, 0, this );
	this->curProgress->show ();
	
	QStringList currencies;
	currencies << "USD" << "EUR" << "GBP" << "CYN" << "JPY" << "CAD" << "CHF" << "AUD" << "INR" << "NZD" << "ZAR" << "ILS" << "XAU" << "XPT" << "XPD";
	
	this->urlCur->setUrl ( "http://tuetuopay.legtux.org/currency.php?from=" + currencies.at ( this->comboInputUnit->currentIndex () )
						  + "&to=" + currencies.at ( this->comboOutputUnit->currentIndex () ) );
	this->requestCur->setUrl ( *(this->urlCur) );
	requestCur->setRawHeader ( "User-Agent", "TuxUnits" );
	
	this->replyCur = this->networkManager->get ( *(this->requestCur) );
	
	this->curErrorFound = false;
	
	QObject::connect ( replyCur, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(curError(QNetworkReply::NetworkError)) );
	QObject::connect ( replyCur, SIGNAL(finished()), this, SLOT(curFinished()) );
	QObject::connect ( replyCur, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(updateCurDL(qint64, qint64)) );
	
		// request http://api.unit-apps.com/convert.php?from=XAU&to=EUR
}

void TU_Category::calcTime ()
{
	double refToYear[] = { 31557600.0, 525960.0, 8766.0, 365.25, 12.0, 1.0, 0.1, 0.01, 0.001 };
	bool ok = true;
	double result = this->editInput->text ().toDouble ( &ok );
	
	if ( !ok )	{
		QMessageBox::critical ( this, tr ( "Erreur" ), tr ( "Saisie non valide." ) );
		return;
	}
	
	result /= refToYear[this->comboInputUnit->currentIndex ()];
	result *= refToYear[this->comboOutputUnit->currentIndex ()];
	
	this->editOutput->setText ( QString ("%1").arg ( result ));
}

void TU_Category::calcTemps ()
{
	bool ok = true;
	double result = this->editInput->text ().toDouble ( &ok );
	
	if ( !ok )	{
		QMessageBox::critical ( this, tr ( "Erreur" ), tr ( "Saisie non valide." ) );
		return;
	}
	
	switch ( comboInputUnit->currentIndex () ) {
		case 0:		// Celsius
			result += 273.15;	// Conversion en kelvin
			break;
		case 1:		// Fahrenheit
			result = ((result + 459.67) * 5) / 9;	// Conversion en kelvin
			break;
		case 2:		// kelvin
			break;	// Pas de conversion kelvin => kelvin >_<
		default:
			break;
	}
	
	switch ( comboOutputUnit->currentIndex () ) {
		case 0:		// Celsius
			result -= 273.15;
			break;
		case 1:		// Fahrenheit
			result = (result * 9 / 5) - 459.67;
			break;
		case 2:		// kelvin
			break;	// result est d�j� en kelvin
		default:
			break;
	}
	
	this->editOutput->setText ( QString ("%1").arg ( result ));
}

void TU_Category::setPos ( QPoint pos )
{
	this->move ( pos );
	this->actualPos = pos;
}

QPoint TU_Category::pos ()
{
	return this->actualPos;
}

void TU_Category::curError ( QNetworkReply::NetworkError error )
{
	this->curErrorFound = true;
	QMessageBox::critical ( this, tr ( "Erreur lors de la r�cup�ration des taux de change" ),
						   tr ( "V�rifiez votre connexion � Internet et le statut de nos serveurs.<br />Statut de nos serveurs : http://svtsjc.webou.net<br />"
								"Code de l'erreur: " ) + this->replyCur->errorString () );
	this->editOutput->setText ( tr ( "Erreur de r�seau" ) );
	this->replyCur->deleteLater ();
	this->curProgress->deleteLater ();
	
	emit toggleCurrencies ( true );
}

void TU_Category::updateCurDL ( qint64 progress, qint64 total )
{
	if ( total != -1 )	{
		this->curProgress->setMaximum ( total );
		this->curProgress->setValue ( progress );
	}
}

void TU_Category::curFinished ()
{
	if ( !this->curErrorFound )	{
		bool ok = true;
		double result = this->replyCur->readAll ().toDouble ( &ok );
		if ( !ok )	{
			QMessageBox::critical ( this, tr ( "Erreur" ), tr ( "R�ponse incorrecte du serveur. V�rifiez votre connexion r�seau et votre serveur DNS" ) );
			return;
		}
		
		ok = true;
		double input = this->editInput->text ().toDouble ( &ok );
		if ( !ok )	{
			QMessageBox::critical ( this, tr ( "Erreur" ), tr ( "Saisie non valide." ) );
			return;
		}
		
		this->editOutput->setText ( QString ("%1").arg ( result * input ));
	}
	this->replyCur->deleteLater ();
	this->curProgress->deleteLater ();
	
	emit toggleCurrencies ( true );
}

void TU_Category::cancelCur ()
{
	
}



















