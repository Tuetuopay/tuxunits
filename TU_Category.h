/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#ifndef _TU_MESURES_H
#define _TU_MESURES_H

#include <QtGui>
#include <QtNetwork>

enum {
	CAT_MESURES, CAT_VOLUMES, CAT_CURRENCIES, CAT_TIME, CAT_TEMPS
};

class TU_Category : public QLabel
{
	Q_OBJECT
	Q_PROPERTY(QPoint pos READ pos WRITE setPos)
	
public:
	TU_Category ( QWidget *parent = 0, int selectedCategory = CAT_MESURES );
	~TU_Category ();
	
public slots:
	void btnCalculate_clicked ();
	void setPos ( QPoint pos );
	QPoint pos ();
	void curError ( QNetworkReply::NetworkError error );
	void updateCurDL ( qint64 progress, qint64 total );
	void curFinished ();
	void cancelCur ();
	
private:
	QLabel		*lblTitle;
	QLabel		*lblPicture;
	QLabel		*lblInputUnit;
	QLabel		*lblOutputUnit;
	
	QLineEdit	*editInput;
	QLineEdit	*editOutput;
	
	QComboBox	*comboInputUnit;
	QComboBox	*comboOutputUnit;
	
	QPushButton *btnCalculate;
	
	QSpacerItem *spacerHLeft;
	QSpacerItem *spacerHMiddle;
	QSpacerItem *spacerHRight;
	QSpacerItem *spacerVMiddle;
	
	QGridLayout *layMain;
	
	QPoint actualPos;
	
	QUrl			*urlCur;
	QNetworkRequest	*requestCur;
	QNetworkAccessManager	*networkManager;
	QNetworkReply	*replyCur;
	bool			curErrorFound;
	QProgressDialog *curProgress;
	
	int category;
	
	void buildMesures ();
	void buildVolumes ();
	void buildCurrencies ();
	void buildTime ();
	void buildTemps ();
	void calcMesures ();
	void calcVolumes ();
	void calcCurrencies ();
	void calcTime ();
	void calcTemps ();
	
protected:
	

signals:
	void toggleCurrencies ( bool ); 
};

#endif