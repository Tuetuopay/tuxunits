# TuxUnits #

*Yet another unit convertor ...*

***

## Why the fuck ? ##

A former geek friend of mine, [@\_tristan971\_](http://twitter.com/_tristan971_), created a while ago a unit convertor for Mac OS X : MacUnits. Written in Objective C + Cocoa, he forgot cross platforming, thus asking me to create a Linux version.

***

## Screenshots ##

![Home Window](http://up.tuetuopay.fr/TU_1.png)
![Volumes](http://up.tuetuopay.fr/TU_2.png)
![Money](http://up.tuetuopay.fr/TU_3.png)
![Moneys](http://up.tuetuopay.fr/TU_4.png)

***

## Compiling for yourself ##

Start by cloning this repo in here `http://opensource.tuetuopay.fr/tuxunits.git`

*Do I really need to explain how ?*

For all platforms, you will only need a C++ compiler and the Qt SDK 4.7/4.8. 
**NOT COMPATIBLE WITH 4.6 !**

### Windows ###

1. Install the Qt SDK 4.7 / 4.8.
2. Open the Qt Command Prompt
3. Generate makefiles & stuff :
        
        qmake -project
        qmake
        mingw32-make release

4. Go to `release/` and execute `TuxUnits.exe`. You will need several DLLs (shipped in this repo) :

    * QtCore.dll
    * QtGui.dll
    * QtNetwork.dll
    * libgcc_s_dw2-1.dll
    * mingwm10.dll

### Linux ###

1. Install Qt
2. Compile
        
        qmake -project
        qmake
        make

3. Execute TuxUnits : `./TuxUnits`

### OS X ###

1. Install Qt. On Mavericks, you'll need to fix some headers in the frameworks
2. Compile

        qmake -project
        qmake -spec macx-g++
        make

3. Execute `open TuxUnits.app`

***

## Using the precompiled binaries ##

### Windows ###

Clone and execute `release/TuxUnits.exe`

### OS X ###

Clone, install the Qt Framework 4.8 and start `TuxUnits.app`

### Linux ###

Clone and compile ... I am too lazy to recompile it under Linux for now xD

***

## Other stuff ##

This software is published under the GNU GPL license.

Feel free to make pull requests :D

Contact me through Twitter : [@Tuetuopay](http://twitter.com/Tuetuopay)