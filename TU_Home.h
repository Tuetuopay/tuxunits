/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#ifndef _TU_HOME
#define _TU_HOME

#include <QtGui>

class TU_Home : public QWidget {
	
	Q_OBJECT
	
public:
	TU_Home ( QWidget *parent = 0 );
	~TU_Home ();
	void toggleAbout ( bool toggle );
	
public slots:
	void btnAbout_clicked ();
	
signals:
	void aboutClicked ();
	
private:
	QLabel			*lblLogo;
	QLabel			*lblText;
	
	QPushButton		*btnAbout;
	
	QVBoxLayout		*layMain;
	QHBoxLayout		*layBtn;
};

#endif