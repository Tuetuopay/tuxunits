/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#ifndef _TU_MAIN_H
#define _TU_MAIN_H

#include <QtGui>
#include <iostream>

#include "TU_Category.h"
#include "TU_Home.h"
#include "TU_About.h"

#define WIN_HEIGHT this->height ()
#define WIN_WIDTH  this->width ()

class TU_MainWindow : public QLabel {

Q_OBJECT
	
public:
	TU_MainWindow ( QWidget *parent = 0 );
	~TU_MainWindow ();
	
private:
	QLabel			*labelHeader;
	QLabel			*labelMaskLeft;
	QLabel			*labelMaskRight;
	
	QPushButton		*btnHome;
	QPushButton		*btnMesures;
	QPushButton		*btnVolumes;
	QPushButton		*btnCurrencies;
	QPushButton		*btnTime;
	QPushButton		*btnTemps;
	
	QWidget			*header;
	
	QVBoxLayout		*layHeader;
	QHBoxLayout		*layHeaderButtons;
	
	QSpacerItem		*vSpacer;
	
	QPoint			*catMiddlePlace;
	QPoint			*catLeftPlace;
	QPoint			*catRightPlace;
	
	TU_Home			*catHome;
	TU_Category		*catMesures;
	TU_Category		*catVolumes;
	TU_Category		*catCurrencies;
	TU_Category		*catTime;
	TU_Category		*catTemps;
	TU_About		*catAbout;
	bool			btnAboutEnabled;
	
	QEasingCurve	animCurve;
	
	QParallelAnimationGroup	*animation;
	
	QPropertyAnimation *animLToMCat ( QWidget *leftCat );
	QPropertyAnimation *animMToRCat ( QWidget *rightCat );
	QPropertyAnimation *animRToMCat ( QWidget *rightCat );
	QPropertyAnimation *animMToLCat ( QWidget *leftCat );
	
public slots:
	void btnHome_clicked ();
	void btnMesures_clicked ();
	void btnVolumes_clicked ();
	void btnCurrencies_clicked ();
	void btnTime_clicked ();
	void btnTemps_clicked ();
	void btnAbout_clicked ();
	void animationFinished ();
};

#endif