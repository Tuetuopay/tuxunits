/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#ifndef _TU_ABOUT_H
#define _TU_ABOUT_H

#include <QtGui>

class TU_About : public QLabel {
	Q_OBJECT
	
public:
	TU_About ( QWidget *parent = 0 );
	~TU_About ();

private:
	QHBoxLayout		*layMain;
	QVBoxLayout		*layThanks;
	QVBoxLayout		*layLinks;
	
	QLabel			*lblLogo;
	QLabel			*lblThanks;
	
	QPushButton		*btnWebsite;
	QPushButton		*btnLicense;
};

#endif