/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#include "TU_About.h"

TU_About::TU_About ( QWidget *parent ) : QLabel ( parent )
{
	this->setFixedSize ( 536, 180 );
	this->setPixmap ( QPixmap ( ":/images/background-categories.png" ));
	
	lblLogo = new QLabel ();
	lblLogo->setPixmap ( QPixmap ( ":/images/logo-128.png" ) );
	
	lblThanks = new QLabel ( tr ( "<span style=\"color: #FFFFFF; font-size: 10px; text-align: center\"><span style=\"font-size: 30px\">TuxUnits</span><br />"
								  "           � UnitApps<br />"
								  "<h4>Version Linux par Alexis Bauvin</h4><br />"
								  "Remerciements particuliers � :<br />"
								  "Tristan Deloche (Design, Graphismes, id�e<br />de d�part...)<br />"
								  "Yonathan Cohen (Serveur des cours mon�taires)</span>" ) );
	
	btnWebsite = new QPushButton ( tr ( "Unit Apps" ) );
	btnLicense = new QPushButton ( tr ( "Conditions g�n�rales\nd'utilisation" ) );
	
	layLinks = new QVBoxLayout;
	layLinks->addStretch ();
	layLinks->addWidget ( btnWebsite );
	layLinks->addStretch ();
	layLinks->addWidget ( btnLicense );
	layLinks->addStretch ();
	
	layMain = new QHBoxLayout;
	layMain->setContentsMargins(2, 5, 2, 0);
	layMain->setSpacing ( 2 );
	layMain->addWidget ( lblLogo );
	layMain->addWidget ( lblThanks );
	layMain->addLayout ( layLinks );
	
	this->setLayout ( layMain );
	
	this->show ();
}

TU_About::~TU_About ()
{
	
}

