/****************************************************************
 *																*
 * This program is under GNU General Public License				*
 *																*
 * If the license isn't included, please go here :				*
 * http://www.gnu.org/licenses/gpl.txt							*
 *																*
 * (c) 2011 Tuetuopay <tuetuopay@tuetuopay.fr>					*
 *																*
 * Project repo : http://opensource.tuetuopay.fr/tuxunits.git	*
 *																*
 ****************************************************************/

#include "TU_Home.h"

TU_Home::TU_Home ( QWidget *parent ) : QWidget ( parent )
{
	this->setFixedSize ( 536, 180 );
	
	this->lblLogo = new QLabel;
	this->lblLogo->setPixmap ( QPixmap ( ":/images/logo-128.png" ) );
	this->lblLogo->setAlignment ( Qt::AlignCenter );
	
	this->lblText = new QLabel ( tr ( "<span style=\"color: #FFFFFF\">S�lectionnez une cat�gorie d'unit�s</style>" ) );
	
	this->btnAbout = new QPushButton ( tr ( "� propos de TuxUnits ..." ) );
	
	this->layMain = new QVBoxLayout ();
	layMain->setContentsMargins(8, 0, 8, 0);
	this->layMain->addWidget ( this->lblLogo );
	
	this->layBtn = new QHBoxLayout ();
	this->layBtn->addWidget ( this->lblText );
	this->layBtn->addStretch ();
	this->layBtn->addWidget ( this->btnAbout );
	
	this->layMain->addLayout ( this->layBtn );
	
	this->setLayout ( this->layMain );
	
	this->show ();
	
	QObject::connect ( this->btnAbout, SIGNAL(clicked()), this, SLOT(btnAbout_clicked()) );
}

TU_Home::~TU_Home ()
{
	
}

void TU_Home::btnAbout_clicked ()
{
	emit aboutClicked ();
}

void TU_Home::toggleAbout ( bool toggle )
{
	btnAbout->setEnabled ( toggle );
}

